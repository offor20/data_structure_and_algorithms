def quick_sort(arr):
    """
    input: unsorted random list of integers
    output: returns a sorted list arranged in ascending order of magnitude
    approach: divide the input list into three sub-lists (list 1 has elements
    all with magnitude less than a pivot value,
    list2 has elements all equal to the pivot value
    list3 has elements whose magnitude are each greater than the pivot value)
    the process is repeated until each sub-list is sorted
    the sub-lists are then merged
    """

    if len(arr) < 2:
        return arr
    else:
        pivot = arr[-1]
        l1, l2, l3 = [], [], []
        for el in arr:
            if el < pivot:
                l1.append(el)
            elif el == pivot:
                l2.append(el)
            else:
                l3.append(el)

        return quick_sort(l1) + l2 + quick_sort(l3)



l = [30, 12, 17, -19, 15, 60, -120, 3, 5, 2, 4, 9, 21, 16]

#print(quick_sort(l))
