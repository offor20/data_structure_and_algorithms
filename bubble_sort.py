def bubble_sort(arr):
    swap_happened = True
    while swap_happened:
        swap_happened = False
        #print("Bubble-Sort", arr)
        for i in range(len(arr)-1):
            if arr[i] > arr[i+1]:
                arr[i], arr[i+1] = arr[i+1], arr[i]
                swap_happened = True


array1 = [50, 12,2,34,5,26, 23, 8,10, 3, 15, 11]
#bubble_sort(array1)
