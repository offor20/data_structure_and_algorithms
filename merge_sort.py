def merge_sort(arr1, arr2):
    sorted_array = []
    i, j = 0, 0
    while i < len(arr1) and j < len(arr2):
        if arr1[i] < arr2[j]:
            sorted_array.append(arr1[i])
            i += 1
        else:
            sorted_array.append(arr2[j])
            j += 1

    while i < len(arr1):
        sorted_array.append(arr1[i])
        i += 1
    while j < len(arr2):
        sorted_array.append(arr2[j])
        j += 1
    return sorted_array

#l = [0,2,4,6,8,10]
#m = [1,3,5,7,9,11]

#print(merge_sort(l,m))

def divide_array(arr):
    if len(arr) < 2:
        return arr[:]
    else:
        middle = len(arr)//2
        l = divide_array(arr[:middle])
        m = divide_array(arr[middle:])
        return merge_sort(l,m)

arr = [6, 4,7,3, 18, 0, -15, 65, 23, 14]

#print(divide_array(arr))
