def insertion_sort(arr):
    """
    an algorithm to sor an array by insertion using a key
    """
    key = 1

    while key < len(arr):
        for i in range(len(arr) -1):
            if arr[i] > arr[key]:
                arr[key], arr[i] = arr[i], arr[key]
                if arr[key] < arr[i -1]:
                    arr[key], arr[i-1] = arr[i -1], arr[key]
                    if arr[key] < arr[i -2]:
                        arr[key], arr[i-2] = arr[i -2], arr[key]

        key += 1
    return arr

l = [20, 2, 18, 3, 16, 5, 14, 7, 12, 9, 10, 11, 8, 13, 6, 15, 4, 17, 19, 1]
array1 = [10, 13, 2, 4, 3, 9, 7, 8, 6, 5, 1]
#print(insertion_sort(l))
