def n_th_term_of_fibonacci_series(n):
    if n == 1:
        return 0
    elif n == 2:
        return 1
    else:
        return n_th_term_of_fibonacci_series(n-1) + n_th_term_of_fibonacci_series(n-2)


n = 9
#n = 2
#n = 7

print(f"The {n}th term of the fibonacci series is: {n_th_term_of_fibonacci_series(n)}")
