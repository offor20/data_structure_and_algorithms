def countdown_timer(n):
    import time
    if n == 0:
        return n
    else:
        print(n)
        time.sleep(1)
        return countdown_timer(n-1)


num = 6
print(countdown_timer(num))
