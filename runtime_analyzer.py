import time
from quick_sort import quick_sort
from random import randint
from bubble_sort import bubble_sort
from merge_sort import divide_array
from selection_sort import selection_sort


#print(l)



#l = [6, 2, 7, 1, 17, 16, 15, 0, 19, -12, 16]
def analyser(algorithm):
    max = 1000000
    l = []
    for num in range(max):
        l.append(randint(2, max))


    start = time.time()
    algorithm(l)

    end = time.time()

    duration = end - start
#print(quick_sort(l))
    if algorithm == quick_sort:
        print(f"The runtime for Quicksort is : {duration}")
    elif algorithm == bubble_sort:
        print(f"The runtime for Bubblesort is : {duration}")
    elif algorithm == divide_array:
        print(f"The runtime for Mergesort is : {duration}")
    elif algorithm == selection_sort:
        print(f"The runtime for Selectionsort is : {duration}")

#analyser(bubble_sort)
analyser(quick_sort)
analyser(divide_array)
